def test_Prove_prime(p):
    Cert = Prove_prime(p)
    return Check_prime(p, Cert) == 'Accept'

def Gen_curve(p):
    while True:
        a = randint(0, p - 1)
        b = randint(0, p - 1)
        E = EllipticCurve(GF(p), [a, b])
        d = 4*Mod(a^3, p) + 27*Mod(b^2, p)
        if gcd(d, p) == 1 and E.order() % 2 == 0:
            break

    q = E.order() // 2
    if q % 2 == 0 or q % 3 == 0 or not MillerRabin(p, q):
        return Gen_curve(p)
    return a, b, q

# вероятностный алгоритм Миллера—Рабина проверки q на простоту
# сложность: O(log(p))
def MillerRabin(p, a):
    k = 0
    q = p - 1
    while q % 2 == 0:
        k += 1
        q //= 2

    a = pow(a, q, p)
    if a == 1: 
        return True # возможно простое

    for _ in range(k):
        if a == p - 1:
            return True # возможно простое
        a = pow(a, 2, p)

    return False # составное

# возвращает точку L ∈ E(Fp) порядка q
#
# https://www.mat.uniroma2.it/~geo2/goldwasserkilian.pdf p. 460
#
# O(log^3(p))

def Find_point(p, q, a, b):
    global cc
    while True:
        x = randint(0, p - 1)
        z = Mod(x^3 + a*x + b, p)
        if z.is_square():
            break

    y = z.nth_root(2)

    E = EllipticCurve(GF(p), [a, b])
    L = E(x, y)

    # элементы либо порядка 2, либо порядка q (их больше)
    if q*L == E(0, 1, 0):
        return L

    return Find_point(p, q, a, b)
    

def Check_prime(p, cert):
    p0 = p
    for (a, b), L, pi in cert:
        if not p0 % 2: raise Exception
        if not p0 % 3: raise Exception
        if not gcd(4*a^3 + 27*b^2, p0): raise Exception
        if not pi > pow(sqrt(sqrt(p0)) + 1, 2): raise Exception
        if not str(L) != '(0 : 1 : 0)': raise Exception
        if not str(pi*L) == '(0 : 1 : 0)': raise Exception
        p0 = pi
    return 'Accept'

# https://crypto-kantiana.com/semyon.novoselov/teaching/elliptic_curves_2021/lecture8_slides.pdf

def Prove_prime(p):
    """
    TESTS::
      sage: test_Prove_prime(1000003)
      True
    
      sage: test_Prove_prime(100000000003)
      True
    """
    i = 0
    pi = p
    C = []
    lp = pow(log(p), log(log(p)))
    global cc
    while pi > p // 5:
        a, b, q = Gen_curve(pi)
        cc = 0
        L = Find_point(pi, q, a, b)
        i += 1
        if i >= lp or pi % 2 == 0 or pi % 3 == 0 or not is_prime(q):
            return Prove_prime(p)
        C.append(((a, b), L, q))
        pi = q

    return C
