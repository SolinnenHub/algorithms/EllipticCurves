from random import randrange as rand

def factor(a, N, Q, P):
    if Q == [0, 1, 0]:
        return False, P
    if P == [0, 1, 0]:
        return False, Q

    x1, y1, _, x2, y2, _ = P + Q
    
    if x1 == x2:
        if y1 == -y2:
            return False, [0, 1, 0]

        d = gcd(x1 - x2, N)
        if d > 1 and d != N:
            return True, d

        d = gcd(y1 + y2, N)
        if d > 1:
            return True, d

        v = Mod((3*x1^2 + a) / (y1 + y2), N)
    else:
        d = gcd(x1 - x2, N)
        if d > 1 and d != N:
            return True, d

        v = Mod((y2 - y1) / (x2 - x1), N)

    x3 = Mod(v^2 - x1 - x2, N)
    y3 = Mod(v*x1 - v*x3 - y1, N)

    return False, [x3, y3, 1]


def multiplier(a, N, P, k):
    Q = [0, 1, 0]
    for b in [int(b) for b in bin(k)[2:]]:
        is_factor, Q = factor(a, N, Q, P=Q)
        if is_factor:
            return True, Q

        if b == 1:
            is_factor, Q = factor(a, N, Q, P)
            if is_factor:
                return True, Q
            
    return False, Q


# Алгоритм факторизации ЕСМ
# https://crypto-kantiana.com/semyon.novoselov/teaching/elliptic_curves_2023/lecture7_slides.pdf
def factorECM(N):
    """
        TESTS::
        sage: factorECM(100070000190133)
        [10007, 10000000019]

        sage: factorECM(100181800505809010267)
        [5009090003, 20000000089]

        sage: factorECM(6986389896254914969335451)
        [833489857, 8382093480298843]
    """
    # из замечания к оптимальному времени работы
    b1 = ceil(exp(1/sqrt(2)*log(N)^0.5*log(log(N))^0.5))

    while True:
        a, x, y = [rand(2, N), rand(2, N), rand(2, N)]

        g = gcd(4*a^3 + 27*(y^2 - x^3 - a*x)^2, N)

        if g == 1:
            P = [x, y, 1]
            p = 1
            while True:
                p = next_prime(p)
                if p > b1:
                    break
                e = 1 
                pe = p
                while pe < b1:
                    is_factor, P = multiplier(a, N, P, pe)
                    if is_factor:
                        return sorted([int(P), N // int(P)])
                    e += 1
                    pe = pow(p, e)
        else:
            return g
            