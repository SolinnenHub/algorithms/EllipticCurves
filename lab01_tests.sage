import random

def jInvariant(a1, a2, a3, a4, a6, q):
    """
    Check is the curve y^2 + a1*x*y + a3*y = x^3 + a2*x^2 + a4*x+a6 is elliptic
    If yes, compute its j-invariant
    If no, raise an error
      :param a1, a2, a3, a4, a6: the coeffs of the input curve
      :param q: char=size of the base field 

    !!! Not tested if q is non-prime
    
    TESTS::
        sage: jInvariant(1, 2, 1, 5, 1, 0)
        6128487/5329

        sage: jInvariant(1, 2, 1, 5, 1, 5)
        3

        sage: jInvariant(0, 1, 0, 0, 0, 0)
        Exception: the curve has a node
    """
    if q != 0:
        if not is_prime(q):
            return print("q is not a prime number")
        else:
            d2 = (pow(a1, 2, q) + 4*a2) % q
            d4 = (2*a4 + a1*a3) % q
            d6 = (pow(a3, 2, q) + 4*a6) % q
            d8 = (pow(a1, 2, q)*a6 + 4*a2*a6 - a1*a3*a4 + a2*pow(a3, 2, q) - pow(a4, 2, q)) % q
            c4 = (pow(d2, 2, q) - 24*d4) % q
            delta = (-pow(d2, 2, q)*d8 - 8*pow(d4, 3, q) - 27*pow(d6, 2, q) + 9*d2*d4*d6) % q

            if delta == 0:
                if c4 == 0:
                    return print("Exception: the curve has a cusp")
                else:
                    return print("Exception: the curve has a node")
            else:
                jE = (pow(c4, 3, q) * pow(delta, -1, q)) % q
                return jE
    else:
        d2 = (pow(a1, 2) + 4*a2)
        d4 = (2*a4 + a1*a3)
        d6 = (pow(a3, 2) + 4*a6)
        d8 = (pow(a1, 2)*a6 + 4*a2*a6 - a1*a3*a4 + a2*pow(a3, 2) - pow(a4, 2))
        c4 = (pow(d2, 2) - 24*d4)
        delta = (-pow(d2, 2)*d8 - 8*pow(d4, 3) - 27*pow(d6, 2) + 9*d2*d4*d6)

        if delta == 0:
            if c4 == 0:
                return print("Exception: the curve has a cusp")
            else:
                return print("Exception: the curve has a node")
        else:
            jE = c4^3 / delta
            return jE

def randIsomorphic(a1, a2, a3, a4, a6, q):
    """
    If a_i's define an elliptic curve E, output the coeffs of a random curve isomorphic to E over F_q
    or over QQ (if q = 0)
    :param a1, a2, a3, a4, a6: the coeffs of the input curve
    :param q: char (=size) of the base field !!! Not tested if q is non-prime
    
    TESTS::
        sage: test_randIsomorphic(0, 1, 0, 0, 1, 0)
        True

        sage: test_randIsomorphic(1, 2, 1, 5, 1, 5)
        True
        
        sage: randIsomorphic(0, 0, 0, 0, 0, 5)
        Exception: the input curve is singular
    """
    if q != 0:
        K = GF(q)

        try:
            EllipticCurve(K, [a1, a2, a3, a4, a6])
        except Exception:
            return print("Exception: the input curve is singular")

        u, r, s, t = [random.randint(1, q-1) for _ in range(4)]

        a_1 = mod((a1 + 2*s) * pow(u, -1, q), q)
        a_2 = mod((a2-s*a1+3*r-s^2)/u^2, q)
        a_3 = mod((a3+r*a1+2*t)/u^3, q)
        a_4 = mod((a4-s*a3+2*r*a2-(t+r*s)*a1+3*r^2-2*s*t)/u^4, q)
        a_6 = mod((a6+r*a4+r^2*a2+r^3-t*a3-t^2-r*t*a1)/u^6, q)

        jE1 = jInvariant(a1, a2, a3, a4, a6, q)
        jE2 = jInvariant(a_1, a_2, a_3, a_4, a_6, q)

        if jE1 == jE2:
            return [K(a_1), a_2, a_3, a_4, a_6]
        else:
            return randIsomorphic(a1, a2, a3, a4, a6, q)

    else:
        try:
            EllipticCurve(QQ, [a1, a2, a3, a4, a6])
        except:
            return print("Exception: the input curve is singular")

        u, r, s, t = [random.randint(1, 100) for _ in range(4)]

        a_1 = int((a1+2*s) / u)
        a_2 = int((a2-s*a1+3*r-s^2) / u^2)
        a_3 = int((a3+r*a1+2*t) / u^3)
        a_4 = int((a4-s*a3+2*r*a2-(t+r*s)*a1+3*r^2-2*s*t) / u^4)
        a_6 = int((a6+r*a4+r^2*a2+r^3-t*a3-t^2-r*t*a1) / u^6)

        try:
            EllipticCurve(QQ, [a_1, a_2, a_3, a_4, a_6])
        except:
            return randIsomorphic(a1, a2, a3, a4, a6, q)

        jE1 = jInvariant(a1, a2, a3, a4, a6, q)
        jE2 = jInvariant(a_1, a_2, a_3, a_4, a_6, q)
        if jE1 == jE2:
            return [a_1, a_2, a_3, a_4, a_6]
        else:
            return randIsomorphic(a1, a2, a3, a4, a6, q)

def test_randIsomorphic(a1, a2, a3, a4, a6, q):
    if q == 0:
        E1 = EllipticCurve([a1, a2, a3, a4, a6])
    else:
        K = GF(q)
        E1 = EllipticCurve([K(a1), a2, a3, a4, a6])
    E2 = EllipticCurve(randIsomorphic(a1, a2, a3, a4, a6, q))
    return E1.is_isomorphic(E2)

def isIsomorphic(a1, a2, a3, a4, a6, b1, b2, b3, b4, b6, q):
    """
    If a_i's and b_i's define elliptic curves E1, E2, solve a system of non-lin. equations to find
    [u,r,s,t] over F_q / QQ that define an isomorphism btw. E1 and E2. It returns all the u's found and
    one tuple  [u,r,s,t] if there is at least one u.
    
    :param a1, a2, a3, a4, a6: the coeffs of the input curve E1
    :param b1, b2, b3, b4, b6: the coeffs of the input curve E2
    :param q: char (=size) of the base field
    
    !!! Not tested if q is non-prime
    !!! Not implemented for j_inv = 0, 1728
    
    TESTS::
        sage: isIsomorphic(0,1,3,1,0,7706571724/19547240159, -133630307391190597856/3438851380502601107529, 52923479675/201660161417379101919146238171333, -510738511897151494016/11825698817184645424683867953329377420485841, 195487310213712167833665086975/40666820702883394956306193463183779725021510167778808819862996889,0)
        ([58641720477, -58641720477],
        [58641720477, 5803716513, 11559857586, 26461739836])
        
        sage: isIsomorphic(0, 1, 3, 1, 2, 27, 18, 1, 43, 38, 53)
        ([48, 5], [48, 21, 12, 42])
        
        sage: isIsomorphic(0,1,3,1,2,12,19,17,10,10, 53)
        'non-isomorphic'
    """
    if q != 0:
        try:
            EllipticCurve(GF(q), [a1, a2, a3, a4, a6])
            EllipticCurve(GF(q), [b1, b2, b3, b4, b6])
        except Exception:
            return "The input curve is singular!"

        uu = []
        # solve a system of non-lin. equations to find [u,r,s,t] over F_q
        for i in [0, 1]:
            for u in list(GF(q))[::-1]:
                s = mod((u * b1 - a1) / 2, q)
                r = mod((u^2 * b2 - a2 + s*a1 + s ^2) / 3, q)
                t = mod((u^3 * b3 - a3 - r*a1) / 2, q)
                a4p = (a4 - s*a3 + 2*r*a2 - a1*(t + r*s) + 3*r^2 - 2*s*t)
                a6p = (a6 + r*a4 +r^2*a2 +r^3 - t*a3 - t^2 - r*t*a1)

                if (b4*u^4 == a4p) and (b6*u^6 == a6p):
                    if i == 0:
                        uu.append(u)
                    else:
                        return uu, [u, r, s, t]

        return "non-isomorphic"

    else:
        try:
            EllipticCurve([a1, a2, a3, a4, a6])
            EllipticCurve([b1, b2, b3, b4, b6])
        except Exception:
            return "The input curve is singular!"
        
        u, r, s, t = var('u, r, s, t')
        eq1 = (s == (u*b1 - a1) / 2)
        eq2 = (r == (u^ 2 *b2 - a2 + s*a1 + s^ 2) / 3)
        eq3 = (t == (u^ 3*b3 - a3 - r*a1) / 2)
        eq4 = (b6 * u^6 == a6 + r*a4 +r^2*a2 +r^3 - t*a3 - t^2 - r*t*a1)
        eq5 = (b4 * u^4 == a4 - s*a3 + 2*r*a2 - a1*(t + r*s) + 3*r^ 2 - 2*s*t)

        # solve a system of non-lin. equations to find [u,r,s,t] over QQ
        solutions = solve([eq1, eq2, eq3, eq4, eq5], u, r, s, t)
        u_ = [solution[0].right() for solution in solutions][::-1]
        urst = [solutions[-1][i].right() for i in range(4)]

        if all(urst[i] in QQ for i in range(len(urst))):
            return u_, urst

        return 'non-isomorphic'

def findExtension(a1, a2, a3, a4, a6, b1, b2, b3, b4, b6, q):
    """
    If a_i's and b_i's define elliptic curves E1, E2, solve a system of non-lin. equations to find
    [u,r,s,t] over F_q / QQ that define an isomorphism btw. E1 and E2.
    If q != 0 and no solution over F_q found, constructs an extension of F_q by adjoing a root of quadratic polynomial
    Similar for QQ.
    
    If the curves are isomorphic, the function returns one tuple  [u,r,s,t] either over the base field,
    or its quadratic extension
    
    
    :param a1, a2, a3, a4, a6: the coeffs of the input curve E1
    :param b1, b2, b3, b4, b6: the coeffs of the input curve E2
    :param q: char (=size) of the base field
    
    !!! Not tested if q is non-prime
    !!! Not implemented for j_inv = 0, 1728
    
    TESTS::
    sage: findExtension(0, 1, 3, 1, 2, 4, 48, 9, 16, 24, 53)
    ('E1, E2 are isomorphic over the base field', [44, 8, 35, 4])
    
    sage: findExtension(0, 1, 3, 1, 2, 47, 45, 15, 39, 8, 53)
    ('E1, E2 are isomorphic over', Univariate Quotient Polynomial Ring in alpha over Ring of integers modulo 53 with modulus alpha^2 + 5, [alpha, 51, 50*alpha, 42*alpha + 25])
    
    sage: findExtension(1, 3, 0, 7, 11, 149285191107/32287120829, -5571517070849439150752/1042458171426445647241, 214924457885/33657972940024045898471277482789, -11715167770447055620209/1086719039173768740089384002472795410912081, 54783704351463028601416544457970/1132859142431390916000129067971120246444103784564827936191218521,0)
    ('E1, E2 are isomorphic over the base field', [32287120829, 37979606869, 74642595553, 88472425508])

    sage: findExtension(1,2,3,4,5, 23/7,-13/7,50/343,-4/2401,2031/117649, 0)
    ('E1, E2 are isomorphic over the base field', [7, 13, 11, 17])

    sage: findExtension(1,2,3,4,5, 1,2,3,248904403/16,2094318345083/64, 0)
    ('E1, E2 are isomoirphic over', Number Field in alpha with defining polynomial x^2 - 1/2020, [alpha, -6057/8080, 1/2*alpha - 1/2, 3/4040*alpha - 18183/16160])
    """
    try:
        if q != 0:
            EllipticCurve(GF(q), [a1, a2, a3, a4, a6])
            EllipticCurve(GF(q), [b1, b2, b3, b4, b6])
        else:
            EllipticCurve([a1, a2, a3, a4, a6])
            EllipticCurve([b1, b2, b3, b4, b6])
    except:
        return("Exception: the input curve is singular")

    if q != 0:
        c = 0
        for u in GF(q): # brutrforce coefficients
            if u == 0:
                continue
            s = (b1*u-a1) / 2
            r = (u^2*b2-a2+s^2+s*a1) / 3
            t = (u^3*b3-a3-r*a1) / 2
            if b6 == (a6+r*a4+r^2*a2+r^3-t*a3-t^2-r*t*a1) / u^6: # check isomorphism
                result = [u, r, s, t]
                c += 1
        if c > 0:
            return 'E1, E2 are isomorphic over the base field', result

        # Otherwise try to extend the field
        for i in range(5, 53):
            try:
                R = PolynomialRing(Integers(q), 'alpha')
                S = R.quotient(f'alpha^2 + {i}', 'alpha')
                for u in S:
                    if u == 0:
                        continue
                    s = (b1*u-a1) / 2
                    r = (u^2*b2-a2+s^2+s*a1) / 3
                    t = (u^3*b3-a3-r*a1) / 2
                    if b6 == (a6+r*a4+r^2*a2+r^3-t*a3-t^2-r*t*a1) / u^6:
                        return 'E1, E2 are isomorphic over', S, [u, r, s, t]
                if c > 0:
                    return 'E1, E2 are isomorphic over', S, [u, r, s, t]
            except:
                pass
                
    else:
        u = var('u')
        r = var('r')
        s = var('s')
        t = var('t')
        solutions = solve([
            b1==(a1+2*s)/u,
            b2==(a2-s*a1+3*r-s^2)/u^2,
            b3==(a3+r*a1+2*t)/u^3,
            b4==(a4-s*a3+2*r*a2-(t+r*s)*a1+3*r^2-2*s*t)/u^4,
            b6==(a6+r*a4+r^2*a2+r^3-t*a3-t^2-r*t*a1)/u^6
        ], u, r, s, t)

        u_ = [solution[0].right() for solution in solutions]
        urst = [solutions[-1][i].right() for i in range(len(solutions[-1]))]

        for v in urst:
            if v in QQ:
                return 'E1, E2 are isomorphic over the base field', urst
            else: 
                E = NumberField(QQ[urst[0]].polynomial(), 'alpha')
                u = E.gen() # generator
                s = (u*b1 - a1) / 2
                r = (u^2*b2 - a2 + s*a1 + s^2) / 3
                t = (u^3*b3 - a3 - r*a1) / 2
                return 'E1, E2 are isomoirphic over', E, [u, r, s, t]