def test_nTorsionPoints(n, a, b, q):
    t1 = nTorsionPoints(n, a, b, q)
    F = t1[0][0].parent()
    E = EllipticCurve(F, [F(a), F(b)])
    t2 = [n*E(P) == E(0) for P in t1]
    return [len(set(t1)), len(set(t2)), t2[0]]

def psy(n, a, b, q, r):
    x, y = var('x, y')
    if n == 0:
        return 0
    if n == 1:
        return 1
    if n == 2:
        if r: return 4*(x^3+a*x+b)
        return 2*y
    if n == 3:
        return 3*x^4+6*a*x^2+12*b*x-a^2
    if n == 4:
        if r: return 8*(x^3+a*x+b)*(x^6+5*a*x^4+20*b*x^3-5*a^2*x^2-4*a*b*x-8*b^2-a^3)
        return 4*y*(x^6+5*a*x^4+20*b*x^3-5*a^2*x^2-4*a*b*x-8*b^2-a^3)
    if n % 2 == 0:
        if r: return (1/(4*(x^3+a*x+b)))*psy(n/2,a,b,q,r)*(psy(n/2+2,a,b,q,r)*psy(n/2-1,a,b,q,r)^2-psy(n/2-2,a,b,q,r)*psy(n/2+1,a,b,q,r)^2)
        return (1/(2*y))*psy(n/2,a,b,q,1)*(psy(n/2+2,a,b,q,1)*psy(n/2-1,a,b,q,1)^2-psy(n/2-2,a,b,q,1)*psy(n/2+1,a,b,q,1)^2)
    else:
        return psy((n-1)/2+2,a,b,q,0)*psy((n-1)/2,a,b,q,0)^3-psy((n-1)/2-1,a,b,q,0)*psy((n-1)/2+1,a,b,q,0)^3

# https://crypto-kantiana.com/elena.kirshanova/teaching/curves_2019/lecture3.pdf
# описывают отображение n:P → [n]P
# используются в алгоритме подсчета точек кривой
# используются в вычислениях изогений
def division_polynomial(n, a, b, q):
    x, y = var('x, y')
    p = psy(n, a, b, q, n%2==0)
    expr = x^3 + a*x+b
    for i in range(-100, 100):
        p = p.substitution_delayed(y^i, expr^(i/2))
    
    R.<x> = PolynomialRing(GF(q))
    return R(p)

# https://crypto-kantiana.com/elena.kirshanova/teaching/curves_2019/lecture4.pdf
def nTorsion_extension_deg(n, a, b, q):
    """
    TESTS::
        sage: nTorsion_extension_deg(3, 3, 17, 23)
        2

        sage: nTorsion_extension_deg(2, 1, 11, 41)
        1

        sage: nTorsion_extension_deg(5, 2, 21, 53)
        4

        sage: nTorsion_extension_deg(7, 1, 7, 11)
        21
    """
    poly = division_polynomial(n, a, b, q)

    factor = [i[0] for i in poly.factor()]
    l = lcm([i.degree() for i in factor])

    for fi in factor:
        i_d = fi.degree()
        if l % (2 * i_d) != 0:
            break

    r = [i[0] for i in fi.roots(GF(q^i_d, 'a'))]
    r_ = r[0]^3 + a*r[0] + b

    if r_.is_square() == -1:
        return 2*l

    d_ = lcm(Mod(q, n).multiplicative_order(), i_d)
    if l == d_ or l == d_*n:
        return l
    return 2*l

# https://crypto-kantiana.com/elena.kirshanova/teaching/curves_2019/lecture4.pdf vvv
def nTorsionPoints(n, a, b, q):
    """    
    TESTS::
        sage: test_nTorsionPoints(3, 3, 17, 23)
        [9, 1, True]

        sage: test_nTorsionPoints(2, 1, 11, 41)
        [4, 1, True]

        sage: test_nTorsionPoints(5, 2, 21, 53)
        [25, 1, True]

        sage: test_nTorsionPoints(7, 1, 7, 11)
        [49, 1, True]
    """
    if n == 1:
        return "[0, infinity]"
    
    poly = division_polynomial(n, a, b, q)
    d = nTorsion_extension_deg(n, a, b, q)

    points = []
    for k in poly.factor():
        points += [(r[0], sqrt(r[0]^3 + a*r[0] + b)) for r in k[0].roots(GF(q^d))]
    if n != 2:
        points += [(i, -j) for i, j in points]
    points += [(0, 1, 0)]

    return points