def test_orderBSGS(i):
    q = Primes().next(2^(16*i) + 1)
    a = ZZ.random_element(1, q-1)
    b = ZZ.random_element(1, q-1)
    E = EllipticCurve(GF(q), [a, b])
    return orderBSGS(a, b, q) == E.order()

# https://crypto-kantiana.com/semyon.novoselov/teaching/elliptic_curves_2023/lecture5_slides.pdf

# функция возвращает порядок группы ЭК. (почти) простой порядок даёт стойкость к атаке Полига-Хэллмана

def orderBSGS(a, b, q):
    """
    TESTS::
        sage: [test_orderBSGS(i) for i in range(1, 5)]
        [True, True, True, True]
    """
    E = EllipticCurve(GF(q), [a, b])
    P = E.random_point()
    m = isqrt(isqrt(q)) + 1

    t = E(0)
    L = []
    Lx = set()
    Ly = set()
    for _ in range(0, m+1):
        L.append(t)
        Lx.add(t[0])
        Ly.add(t[1])
        t += P
    
    u = 2*m*P
    z = (q+1)*P - u*m
    for k in range(-m, m+1):
        r = q+1+2*m*k
        if z[0] in Lx:
            if z[1] in Ly:
                M = r - L.index(z)
                break
            else:
                M = r + L.index(-z)
                break
        z += u

    while True:
        for p in factor(M):
            if (M // p[0]) * P == E(0, 1, 0):
                M = int(M // p[0])
                break
        else:
            break

    L = 1
    L = lcm(L, M)
    if 4*isqrt(q) <= L < q+1-2*isqrt(q):
        return L * ((q + 1 + ceil(2*sqrt(q))) // L)
    return L